#!/bin/bash

SLURM_DEP_FILENAME=".slurm_dep_$$"
SLURM_DEP_FILENAME=".slurm_dep"

parsesd() {
    marker="Submitted batch job"
   while IFS= read -r line
   do
     echo "$line"
     echo "$line" | grep "$marker" | sed "s/.*$marker \([0-9]\+\).*/\1/"  >> "${SLURM_DEP_FILENAME}"
   done
}

getsd() {
  arg=afterok:$(cat "${SLURM_DEP_FILENAME}" | tr '\n' ':')
  arg=${arg::-1}
  echo $arg
  rm -f "${SLURM_DEP_FILENAME}"
}
