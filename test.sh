#!/bin/bash

source slurmdep.sh

# Two jobs run independently
sbatch toyjob.sh | parsesd
sbatch toyjob.sh | parsesd

# Job waits for the others to finish
sbatch --dependency=$(getsd) toyjob.sh
