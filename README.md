# Easy slurm dependency management


Instead of running things manually once a job is done, chain the jobs with
dependencies.

```
#!/bin/bash

source slurmdep.sh

# Two jobs run independently
sbatch toyjob_1.sh | parsesd
sbatch toyjob_2.sh | parsesd

# Job waits for the others to finish
sbatch --dependency=$(getsd) merger.sh
```
